# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of Debian Installer templates to French
# Copyright (C) 2004-2009 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
#
# Translations from iso-codes:
# Christian Perrier <bubulle@debian.org>, 2002-2004.
# Pierre Machard <pmachard@debian.org>, 2002-2004.
# Denis Barbier <barbier@debian.org>, 2002-2004.
# Philippe Batailler <philippe.batailler@free.fr>, 2002-2004.
# Michel Grentzinger <mic.grentz@online.fr>, 2003-2004.
# Christian Perrier <bubulle@debian.org>, 2005, 2006, 2007, 2008, 2009, 2010, 2011.
#   Alastair McKinstry <mckinstry@computer.org>, 2001.
#   Cedric De Wilde <daique@tiscalinet.be>, 2001.
#   Christian Perrier <bubulle@debian.org>, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2012, 2013, 2014, 2015, 2016, 2017.
#   Christophe Fergeau <christophe.fergeau@ensimag.imag.fr>, 2000-2001.
#   Christophe Merlet (RedFox) <redfox@eikonex.org>, 2001.
#   Free Software Foundation, Inc., 2000-2001, 2004, 2005, 2006.
#   Grégoire Colbert <gregus@linux-mandrake.com>, 2001.
#   Tobias Quathamer <toddy@debian.org>, 2007, 2008.
# Alban Vidal <alban.vidal@zordhak.fr>, 2018.
# Baptiste Jammet <baptiste@mailoo.org>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: fr\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2020-08-17 10:18+0100\n"
"Last-Translator: Baptiste Jammet <baptiste@mailoo.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "Faut-il charger des pilotes depuis un support amovible ?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr ""
"Aucun périphérique ne contenant de support d'installation n'a été trouvé."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Il peut être nécessaire de charger des pilotes additionnels depuis un "
"support amovible, par exemple une disquette de pilotes ou une clé USB. Si "
"vous avez un tel support, insérez-le puis continuez. Dans le cas contraire, "
"vous aurez la possibilité de choisir vous-même certains modules."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Détection du matériel : recherche de support d'installation"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr ""
"Voulez-vous choisir le module et le périphérique du support d'installation ?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr ""
"Aucun périphérique ne contenant de support d'installation (par exemple un "
"lecteur CD) n'a été trouvé."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Si le lecteur de CD-ROM est un ancien lecteur Mitsumi ou un lecteur qui "
"n'est ni un IDE, ni un SCSI, vous pouvez indiquer quel module devra être "
"chargé et quel périphérique sera utilisé. Si vous ne connaissez pas le "
"module et le pilote dont vous avez besoin, veuillez consulter la "
"documentation ou essayez de procéder à une installation par le réseau."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "Faut-il réessayer de monter le support d'installation ?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Le support d'installation ne peut être monté. Lors d'une installation depuis "
"un disque laser, cela signifie probablement que le disque ne se trouve pas "
"dans le lecteur. Si c'est le cas, vous pouvez l'insérer et recommencer une "
"nouvelle fois."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "Module pour accéder au support d'installation :"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"La détection automatique n'a pas trouvé de support d'installation. Si vous "
"installez à partir d'un disque laser et que vous possédez un lecteur de CD-"
"ROM spécifique (qui n'est ni IDE ni SCSI), vous pouvez essayer de charger un "
"module particulier."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "Fichier de périphérique pour le support d'installation :"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Afin de pouvoir accéder au support d'installation (par exemple le CD-ROM), "
"veuillez indiquer le fichier de périphérique utilisé. En effet, les lecteurs "
"non standards utilisent des fichiers de périphérique non standards tels que /"
"dev/mcdx par exemple."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Vous pouvez obtenir un interpréteur de commandes sur le deuxième terminal "
"(ALT+F2) pour voir les périphériques disponibles dans /dev en tapant « ls /"
"dev ». Vous pourrez revenir à cet écran avec ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Analyse du support d'installation"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Analyse du répertoire ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "Support d'installation détecté"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"La détection automatique du support d'installation s'est bien déroulée. Un "
"lecteur a été identifié et ce lecteur contient ${cdname}. Vous pouvez "
"poursuivre l'installation."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "Support d'installation UNetbootin détecté"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Il semble que le support d'installation a été créé avec UNetbootin. Cette "
"méthode est connue pour provoquer des problèmes complexes et difficiles à "
"reproduire. Si vous rencontrez des difficultés avec ce support "
"d'installation, veuillez d'abord essayer d'utiliser un support qui ne soit "
"pas créé avec UNetbootin avant de signaler ces problèmes."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"Le guide d'installation contient plus d'informations sur la façon de créer "
"un support d'installation USB sans utiliser UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "Support d'installation non valable détecté"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "Le support détecté ne peut pas être utilisé pour l'installation."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "Veuillez insérer un support valable pour continuer l'installation."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Erreur de lecture du fichier « Release »"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Le support d'installation ne semble pas contenir de fichier « Release ». Il "
"est également possible que ce fichier soit présent mais ne puisse pas être "
"lu correctement."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Vous pouvez tenter à nouveau la détection du support. Cependant, même si "
"elle réussit à la deuxième tentative, vous pourriez rencontrer des "
"difficultés lors de la suite de l'installation."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Démontage/éjection du support d'installation..."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "Détection et montage du support d'installation"
