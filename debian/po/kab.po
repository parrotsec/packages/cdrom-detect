# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Kabyle translation for debian-installer
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the debian-installer package.
# Slimane Selyan Amiri <selyan.kab@gmail.com>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer sublevel1\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2020-10-24 15:08+0100\n"
"Last-Translator: Slimane Selyan Amiri <selyan.kab@gmail.com>\n"
"Language-Team: Kabyle <kab@li.org>\n"
"Language: kab\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "Load drivers from removable media?"
msgstr "Sali-d ibenkan seg umidya yettwakkasen?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr "Ulac ibenk i umidya n usebded i yettwafen."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Ilaq ad d-tsaliḍ inuḍaf-nniḍen seg umidyat aziraz, am tḍebsit talwayant n "
"unuḍaf d tsarut USB. Ma yella wejden wigi akka tura, err amidyat syen "
"kemmel. Neɣ ma ulac, ad tesɛuḍ lxetyaṛ ad tferneḍ s ufus kra n yizegrar."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
msgid "Detecting hardware to find installation media"
msgstr "Tifin n warrum i wakken ad d-naf amidyat n usebded"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "Manually select a module and device for installation media?"
msgstr "Fren s ufus azegri d yibenk i umidya n usebded?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "Ulac ibenk i umidya n usebded (am yibenk CD-ROM) i yettwafen."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Ma yella ibenk-inek·inem CD-ROM d Mitsumi aqbur neɣ d ameQri-nniḍen n CD-ROM "
"ur nelli d IDE, ur nelli d SCSI, ilaq ad tferneḍ anwa azegrir ara d-tsaliḍ "
"akked yibenk ara tesqedceḍ. Ma yella ur teẓriḍ ara anwa azegrir d yibenk i "
"ilaqen, wali kra n tsemlit neɣ ɛreḍ asebded seg uzeṭṭa."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid "Retry mounting installation media?"
msgstr "Ales tikkelt-nniḍen aserkeb n umidya n usebded?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Amidya-inek·inem n usebded yegguma ad yettuserkeb. Ma yella tsebdadeḍ seg CD-"
"ROM, yezmer d aḍebsi i ulac deg yibenk. Ihi ma yella d aya, tzemreḍ ad t-"
"terreḍ syen ɛreḍ tikkelt-nniḍen."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid "Module needed for accessing the installation media:"
msgstr "Azegrir yesra anekcum ɣer umidyat n usebded:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Tifin tawurmant ur tufi ara ameɣri i umidyat n usebded. Mi ara tesbeddeḍ seg "
"CD-ROM yerna kečč·kemm ur tesɛiḍ ara ameɣri n CD-ROM armagnu (ur nelli d IDE "
"neɣ d SCSI), tzemreḍ ad tɛerḍeḍ ad d-tsaliḍ azegrir usdid."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid "Device file for accessing the installation media:"
msgstr "Afaylu n yibenk i unekcum ɣer umidya n usebded:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"I wakken ad tkecmeḍ ɣer umidyat-inek·inem n usebded (am CD-ROM inek·inem), "
"ttxil-k·m sekcem afaylu n yibenk i ilaqen ad yettuseqdec. Imeɣriyen n CD-ROM "
"arisluganen seqdacen ifuyla n yibenk araslugan (am /dev/mcdx).)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Tzemreḍ ad tuɣaleḍ ɣer shell deg tdiwent tis snat (ALT+F2) i usenqed n "
"yibenkan yellan deg /dev s \"ls /dev\". Tzemreḍ ad tuɣaleḍ ɣer ugdil-a s "
"usiti ɣef ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
msgid "Scanning installation media"
msgstr "Asmiḍen n umidya n usebded"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "Aḍummu n ${DIR}..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid "Installation media detected"
msgstr "Amidyat n usebded yecceḍ"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"Tifin tawurmant n umidyat n usebded tedda akken iwata. Ameɣri ideg yella "
"'${cdname}' yettwaf. Asebded tura ad ikemmel."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "Amidyat UNetbootin yettwafen"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Yettban wallal-inek·inem n usebded yettusirew s useqdec n UNetbootin. "
"UNetbootin yezga yeqqen ɣer yineqqisen n wuguren i iweɛren neɣ ur nettales "
"ara afares sɣur iseqdac; ma yella tesɛiḍ uguren s useqdec n wallal-a n "
"usebded, ttxil-k·m ɛreḍ asebded i tikkelt-nniḍen war aseqdec n UNetbootin "
"send tuzna n wuguren."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"Amnir n usebded deg-s ugar n talɣut ɣef wamek ara ternuḍ allal n usebded n "
"USB srid war UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Incorrect installation media detected"
msgstr "Asebde arameɣtu n umidya yettwafen"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "The detected media cannot be used for installation."
msgstr "Amidyat i d-yettwafen ulamek yettuseqdac i usebded."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
msgid "Please provide suitable media to continue with the installation."
msgstr "Ttxil-k·m mudd amidya i iwatan i wakken ad tkemmeleḍ asebded."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Tuccḍa deg tɣuri n ufaylu n lqem"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Amidyat n usebded ur yettban ara yella deg-s ufaylu n \"Release\" ameɣtu, "
"neɣ ahat afaylu-a ur d-yettwaɣra ara akken iwata."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Tzemreḍ ad tɛerḍeḍ allus n tifin n umidyat, maca ma yella ur teddi ara "
"tikkelt-a, tzemreḍ ad d-temlileḍ d wuguren ticki deg usebded."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr "Afsay/asufeɣ n umidyat n userkeb..."

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
msgid "Detect and mount installation media"
msgstr "Af-d amidyat n usebded syen serkeb-it"
