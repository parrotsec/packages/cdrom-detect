# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# Irish messages for debian-installer.
# Copyright (C) 2003 Software in the Public Interest, Inc.
# This file is distributed under the same license as debian-installer.
#
# Translations from iso-codes:
# Alastair McKinstry <mckinstry@computer.org>, 2001,2002
# Free Software Foundation, Inc., 2001,2003
# Kevin Patrick Scannell <scannell@SLU.EDU>, 2004, 2008, 2009, 2011.
# Sean V. Kelley <s_oceallaigh@yahoo.com>, 1999
msgid ""
msgstr ""
"Project-Id-Version: debian-installer\n"
"Report-Msgid-Bugs-To: cdrom-detect@packages.debian.org\n"
"POT-Creation-Date: 2019-09-26 22:02+0000\n"
"PO-Revision-Date: 2006-03-21 14:42-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid "Load drivers from removable media?"
msgstr "Luchtaigh tiománaithe ó mheán inbhainte"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
msgid "No device for installation media was detected."
msgstr ""

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:1001
#, fuzzy
msgid ""
"You may need to load additional drivers from removable media, such as a "
"driver floppy or a USB stick. If you have these available now, insert the "
"media, and continue. Otherwise, you will be given the option to manually "
"select some modules."
msgstr ""
"Seans go gcaithfidh tú tiománaithe breise CD-ROM a luchtú ó mheán inbhainte, "
"mar shampla ó dhiosca flapach. Má tá a leithéid de mheán agat anois, cuir é "
"sa tiomántán agus lean ar aghaidh. Mura bhfuil, beidh tú in ann modúil CD-"
"ROM a roghnú de láimh."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:2001
#, fuzzy
msgid "Detecting hardware to find installation media"
msgstr "Ag brath crua-earraí chun tiomántáin diosca a aimsiú"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "Manually select a module and device for installation media?"
msgstr "Roghnaigh modúl agus gléas CD-ROM de láimh?"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid "No device for installation media (like a CD-ROM device) was detected."
msgstr "Níor braitheadh tiomántán coitianta CD-ROM."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:3001
#, fuzzy
msgid ""
"If your CD-ROM drive is an old Mitsumi or another non-IDE, non-SCSI CD-ROM "
"drive, you should choose which module to load and the device to use. If you "
"don't know which module and device are needed, look for some documentation "
"or try a network installation."
msgstr ""
"Seans go bhfuil seantiomántán CD-ROM de chuid Mitsumi agat, nó tiomántán CD-"
"ROM neamh-IDE, neamh-SCSI eile. Sa chás sin, ba chóir duit an modúl le "
"luchtú agus an gléas le húsáid a roghnú. Mura bhfuil a fhios agat cé acu "
"modúl agus gléas atá de dhíth ort, féach ar dhoiciméadú do chórais nó bain "
"triail as suiteáil líonra in ionad suiteála ó CD-ROM."

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid "Retry mounting installation media?"
msgstr "Focal faire suiteála cianda:"

#. Type: boolean
#. Description
#. :sl2:
#: ../cdrom-detect.templates:4001
#, fuzzy
msgid ""
"Your installation media couldn't be mounted. When installing from CD-ROM, "
"this probably means that the disk was not in the drive. If so you can insert "
"it and try again."
msgstr ""
"Níorbh fhéidir do CD-ROM suiteála a fheistiú. Is dócha nach raibh an CD-ROM "
"sa tiomántán. Sa chás sin, cuir é isteach agus bain triail eile as."

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid "Module needed for accessing the installation media:"
msgstr "Modúl de dhíth chun CD-ROM a rochtain:"

#. Type: select
#. Description
#. :sl2:
#: ../cdrom-detect.templates:5001
#, fuzzy
msgid ""
"The automatic detection didn't find a drive for installation media. When "
"installing from CD-ROM and you have an unusual CD-ROM drive (that is neither "
"IDE nor SCSI), you can try to load a specific module."
msgstr ""
"Níor aimsíodh tiomántán CD-ROM go huathoibríoch. Is féidir leat modúl ar "
"leith a luchtú má tá tiomántán neamhchoitianta agat (.i. tiomántán neamh-IDE "
"neamh-SCSI)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid "Device file for accessing the installation media:"
msgstr "Comhad gléis le haghaidh rochtana CD-ROM:"

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
#, fuzzy
msgid ""
"In order to access your installation media (like your CD-ROM), please enter "
"the device file that should be used. Non-standard CD-ROM drives use non-"
"standard device files (such as /dev/mcdx)."
msgstr ""
"Chun do thiomántán CD-ROM a rochtain, iontráil an comhad gléis le húsáid. "
"Úsáideann tiomántáin neamhchaighdeánacha comhaid ghléis neamhchaighdeánacha "
"(mar shampla, /dev/mcdx)."

#. Type: string
#. Description
#. :sl2:
#: ../cdrom-detect.templates:6001
msgid ""
"You may switch to the shell on the second terminal (ALT+F2) to check the "
"available devices in /dev with \"ls /dev\". You can return to this screen by "
"pressing ALT+F1."
msgstr ""
"Is féidir leat blaosc a úsáid ar an dara teirminéal (ALT+F2) chun na "
"gléasanna atá ar fáil a thaispeáint le \"ls /dev\". Ansin, fill ar an "
"scáileán seo le ALT+F1."

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:10001
#, fuzzy
msgid "Scanning installation media"
msgstr "Theip ar shuiteáil SILO"

#. Type: text
#. Description
#. :sl1:
#: ../cdrom-detect.templates:11001
msgid "Scanning ${DIR}..."
msgstr "${DIR} á scanadh..."

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid "Installation media detected"
msgstr "Braitheadh meán UNetbootin"

#. Type: note
#. Description
#. :sl2:
#: ../cdrom-detect.templates:12001
#, fuzzy
msgid ""
"Autodetection of the installation media was successful. A drive has been "
"found that contains '${cdname}'. The installation will now continue."
msgstr ""
"D'éirigh le brath an tiomántáin CD-ROM. Aimsíodh tiomántán CD-ROM agus tá "
"dlúthdhiosca darb ainm ${cdname} ann faoi láthair. Leanfar ar aghaidh leis "
"an tsuiteáil anois."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid "UNetbootin media detected"
msgstr "Braitheadh meán UNetbootin"

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"It appears that your installation medium was generated using UNetbootin. "
"UNetbootin is regularly linked with difficult or unreproducible problem "
"reports from users; if you have problems using this installation medium, "
"please try your installation again without using UNetbootin before reporting "
"issues."
msgstr ""
"Dealraíonn sé gur úsáideadh UNetbootin chun an meán suiteála a chruthú. Is "
"minic gur cúis le fadhbanna deacra é UNetbootin; má tá fadhbanna agatsa leis "
"an meán suiteála seo, molaimid duit iarracht eile a dhéanamh gan UNetbootin "
"a úsáid sula ndéanfaidh tú tuairisc ar an bhfadhb."

#. Type: note
#. Description
#. :sl3:
#: ../cdrom-detect.templates:13001
msgid ""
"The installation guide contains more information on how to create a USB "
"installation medium directly without UNetbootin."
msgstr ""
"Gheobhaidh tú tuilleadh eolais sa lámhleabhar suiteála maidir le meán "
"suiteála USB a chruthú go díreach, neamhspleách ar UNetbootin."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Incorrect installation media detected"
msgstr "Braitheadh meán UNetbootin"

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "The detected media cannot be used for installation."
msgstr ""
"Tá dlúthdhiosca sa tiomántán CD-ROM ach ní féidir é a úsáid le haghaidh "
"suiteála."

#. Type: error
#. Description
#. :sl2:
#: ../cdrom-detect.templates:14001
#, fuzzy
msgid "Please provide suitable media to continue with the installation."
msgstr ""
"Ionsáigh dlúthdhiosca oiriúnach chun leanúint ar aghaidh leis an tsuiteáil."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
msgid "Error reading Release file"
msgstr "Earráid agus an comhad \"Release\" á léamh"

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"The installation media do not seem to contain a valid 'Release' file, or "
"that file could not be read correctly."
msgstr ""
"Is cosúil nach bhfuil comhad bailí \"Release\" ar an CD-ROM, nó níorbh "
"fhéidir an comhad sin a léamh i gceart."

#. Type: error
#. Description
#. Translators: DO NOT TRANSLATE "Release". This is the name of a file.
#. :sl2:
#: ../cdrom-detect.templates:15001
#, fuzzy
msgid ""
"You may try to repeat the media detection, but even if it does succeed the "
"second time, you may experience problems later in the installation."
msgstr ""
"Is féidir leat an tiomántán CD-ROM a bhrath arís, ach seans go mbeidh "
"fadhbanna níos déanaí sa tsuiteáil, fiú má éiríonn tú leis an dara huair."

#. Type: text
#. Description
#. finish-install progress bar item
#. :sl1:
#: ../cdrom-detect.templates:19001
msgid "Unmounting/ejecting installation media..."
msgstr ""

#. Type: text
#. Description
#. Item in the main menu to select this package
#. Translators: keep below 55 columns.
#. :sl2:
#: ../cdrom-detect.templates:20001
#, fuzzy
msgid "Detect and mount installation media"
msgstr "Úsáid \"scoir\" chun filleadh ar an roghchlár suiteála."
